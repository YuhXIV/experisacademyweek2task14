﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment
{
    public class Palindrome
    {
        public bool IsPalindrome(int prod)
        {
            string input = prod.ToString();

            string reverseInput = "";
            for (int i = input.Length - 1; i >= 0; i--)
            {
                reverseInput += input.Substring(i, 1);
            }
           
            if(input.Equals(reverseInput)) return true;
            return false;
        }

        public int[] HighestPalindrom()
        {
            int[] res = {0, 0, 0};

            for (int i = 999; i > 99; i--)
            {
                for (int j = 999; j > 99; j--)
                {
                    int prod = j * i;
                    if (IsPalindrome(prod))
                    {
                        if (res[0] < prod)
                        {
                            res[0] = prod;
                            res[1] = i;
                            res[2] = j;
                        }
                    }
                }
            }
            return res;
        }
    }
}
