﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment
{
    class Controller
    {
        int prod;
        Palindrome palindrome = new Palindrome();
        public Controller()
        {
            Console.WriteLine("Finding highest Palindrome....");
            Console.WriteLine($"Highest Palindrome: {palindrome.HighestPalindrom()[0]}");
            Console.WriteLine($"The two 3-digits is: {palindrome.HighestPalindrom()[1]}, {palindrome.HighestPalindrom()[2]}");
        }
    }
}
